﻿using System;

namespace STVNRLND
{
    class STVNRLNDException : Exception
    {
        public STVNRLNDException()
        {
        }

        public STVNRLNDException(string message) : base(message)
        {

        }
    }

    class OutOfBoundsException : STVNRLNDException
    {
        public OutOfBoundsException()
        {
        }

        public OutOfBoundsException(string message) : base(message)
        {

        }
    }
}
